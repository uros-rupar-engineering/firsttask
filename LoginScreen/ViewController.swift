//
//  ViewController.swift
//  LoginScreen
//
//  Created by uros.rupar on 5/10/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        signup.layer.cornerRadius = 20
        signin.layer.cornerRadius = 20
        
            }

    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var secondView: UIView!
    
   
    @IBOutlet weak var signup: UIButton!
    
    @IBOutlet weak var signin: UIButton!
}

